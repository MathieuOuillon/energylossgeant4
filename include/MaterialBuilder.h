//
// Created by ouillon-l on 22/03/2022.
//

#ifndef ENERGYLOSSGEANT4_MATERIALBUILDER_H
#define ENERGYLOSSGEANT4_MATERIALBUILDER_H


#include <vector>
#include <string>
#include <map>
#include "Material.h"
#include "ElementBuilder.h"

class MaterialBuilder {
public:
    MaterialBuilder();

    Material * FindMaterial(const std::string &nameMat);

private:
    void Initialise();

    void AddMaterial(const std::string &nameMat, double dens, std::vector<MyElement *> elmVec, double Imat, int ncomp,
                     double *params, double n_el, std::vector<double> atomicDensity);
    void BuildHelium();
    void BuildCarbonDioxide();
    void BuildBONuSGas();
    void BuildKapton();
    void BuildMylar();


    std::map<std::string, Material * > materialMap;
    ElementBuilder * elementBuilder;
};


#endif //ENERGYLOSSGEANT4_MATERIALBUILDER_H
