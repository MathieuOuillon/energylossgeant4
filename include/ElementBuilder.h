//
// Created by ouillon-l on 23/03/2022.
//

#ifndef ENERGYLOSSGEANT4_ELEMENTBUILDER_H
#define ENERGYLOSSGEANT4_ELEMENTBUILDER_H


#include <map>
#include <string>
#include "MyElement.h"
#include "MySystemOfUnits.h"
#include "MyPhysicalConstants.h"

const int MymaxNumElements = 108;

class ElementBuilder {
public :
    ElementBuilder();
    MyElement * FindElement(const std::string &nameElm);
private:
    void Initialise();
    void AddElement(const std::string &name, const std::string &symbol, double Zeff, const double &A, const double &W);

    void BuildHydrogen();
    void BuildHelium();
    void BuildCarbon();
    void BuildNitrogen();
    void BuildOxygen();

    std::map<std::string, MyElement * > ElementMap;
    double bindingEnergy [MymaxNumElements];
};


#endif //ENERGYLOSSGEANT4_ELEMENTBUILDER_H
