//
// Created by ouillon-l on 14/03/2022.
//

#ifndef ENERGYLOSSGEANT4_MATERIAL_H
#define ENERGYLOSSGEANT4_MATERIAL_H


#include <vector>
#include "MyElement.h"
#include "MyPhysicalConstants.h"

class Material {

public:

    Material(const std::string &name,                   //its name
             double density,                            //density
             int nComponents,                           //nbOfComponents
             double I,                                  //MeanExitationValue
             double n_el,                               //ElectronDensity
             double temp = MyCLHEP::STP_Temperature,    //temperature
             double pressure = MyCLHEP::STP_Pressure);  //pressure

    inline std::vector<MyElement *> &getMyElementVector() { return MyElementVector; }

    inline void
    setMyElementVector(const std::vector<MyElement *> &myElementVector) { MyElementVector = myElementVector; }

    void setIonisParamMat(double * params);

    inline double getDensity() const {return fDensity;}

    inline double GetMeanExcitationEnergy() const { return fI; };

    inline double  GetElectronDensity() const {return fElectronDensity;}

    inline double getfCdensity() const {return fCdensity;}
    inline double getfMdensity() const {return fMdensity;}
    inline double getfAdensity() const {return fAdensity;}
    inline double getfX0density() const {return fX0density;}
    inline double getfX1density() const {return fX1density;}
    inline double getfD0density() const {return fD0density;}

    inline const std::vector<double> &getAtomicDensity() const {
        return AtomicDensity;
    }

    inline void setAtomicDensity(const std::vector<double> &atomicDensity) {
        AtomicDensity = atomicDensity;
    }

    inline double getTotAtomicDensity() const {
        double totAtomicDensity = 0;
        for (double i : AtomicDensity) {
            totAtomicDensity += i;
        }
        return totAtomicDensity;
    }

private:

    std::string fName;
    double fI;
    double fZeff;
    double fAeff;
    double fDensity;
    int fNbComponents;
    double fElectronDensity;
    double fTemp;                  // Temperature (defaults: STP)
    double fPressure;              // Pressure    (defaults: STP)

    std::vector<MyElement *> MyElementVector;
    std::vector<double> AtomicDensity;

    // parameters of the density correction
    double fCdensity;
    double fMdensity;
    double fAdensity;
    double fX0density;
    double fX1density;
    double fD0density;

};


#endif //ENERGYLOSSGEANT4_MATERIAL_H
