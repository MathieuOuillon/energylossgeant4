
#ifndef ENERGYLOSSGEANT4_PARTICLE_H
#define ENERGYLOSSGEANT4_PARTICLE_H

#include <string>
#include <utility>
#include "MyPhysicalConstants.h"

class Particle {
public:
    Particle(std::string  aName, double mass, double spin, double charge);
    virtual ~Particle() = default;

    [[nodiscard]] inline const std::string& GetParticleName() const { return theParticleName; }
    [[nodiscard]] inline double GetMass() const { return theMass; }
    [[nodiscard]] inline double GetCharge() const { return theCharge; }
    [[nodiscard]] inline double GetSpin() const { return theSpin; }


private:
    std::string theParticleName;
    double theMass;
    double theCharge;
    double theSpin;


};

class Proton : public Particle{
public:
    explicit Proton(std::string aName = "Proton",
           double mass = MyCLHEP::proton_mass_c2,
           double spin = 1,
           double charge = MyCLHEP::eplus) : Particle(std::move(aName),mass,spin,charge){};

};


#endif
