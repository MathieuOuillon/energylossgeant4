
#ifndef ENERGYLOSSGEANT4_MYATOMICSHELLS_H
#define ENERGYLOSSGEANT4_MYATOMICSHELLS_H


class MyAtomicShells {
public :  //with description

    static int    GetNumberOfShells(int Z);
    static int    GetNumberOfElectrons(int Z, int SubshellNb);

private :

    static const int    fNumberOfShells[101];
    static const int    fIndexOfShells[101];
    static const int    fNumberOfElectrons[1540];
    static const double fBindingEnergies[1540];
};


#endif
