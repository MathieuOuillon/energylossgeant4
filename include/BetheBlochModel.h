//
// Created by ouillon-l on 14/03/2022.
//

#ifndef ENERGYLOSSGEANT4_BETHEBLOCHMODEL_H
#define ENERGYLOSSGEANT4_BETHEBLOCHMODEL_H


#include <cfloat>
#include "Particle.h"
#include "Material.h"
#include "MyPhysicsFreeVector.h"

class BetheBlochModel {


public:
    explicit BetheBlochModel();

    ~BetheBlochModel();

    void InitalizeShellCorrection();

    double KShell(double tet, double eta);

    double ComputeDEDXPerVolume(Particle *p, Material *material, double kineticEnergy, bool correction);

private:

    MyPhysicsFreeVector *sBarkasCorr = nullptr;
    MyPhysicsFreeVector *sThetaK = nullptr;
    MyPhysicsFreeVector *sThetaL = nullptr;
    int nK = 20;

    int nL = 26;
    int nEtaK = 29;
    int nEtaL = 28;

    const Particle * particle = nullptr;
    double mass = 0.0;
    double spin = 0.0;
    double chargeSquare = 1.0;
    double ratio = 1.0;
    double twoln10;

    void SetupParameters(const Particle* p);

    int Index(double x, const double *y, int n) const;

    double Value(double xv, double x1, double x2,
                 double y1, double y2) const;

    double Value2(double xv, double yv, double x1, double x2,
                  double y1, double y2, double z11, double z21,
                  double z12, double z22) const;

    static const double ZD[11];
    static const double UK[20];
    static const double VK[20];
    static double ZK[20];
    static const double Eta[29];
    static double CK[20][29];
    static double CL[26][28];
    static const double UL[26];
    static double VL[26];

    double LShell(double tet, double eta);


    double MaxSecondaryEnergy(double kinEnergy) const;

    double GetDensityCorrection(double x, Material *material) const;

    double ShellCorrection(Material *material, double kineticEnergy);

    double BarkasCorrection(Particle *p, Material *material, double kineticEnergy);

    double HighOrderCorrections(Particle *p, Material *material, double kineticEnergy, double);

    double BlochCorrection(Particle *p, double kineticEnergy) const;

    double MottCorrection(Particle *p, double kineticEnergy) const;
};


inline int BetheBlochModel::Index(double x, const double *y, int n) const {
    int iddd = n - 1;
    do { --iddd; } while (iddd > 0 && x < y[iddd]);
    return iddd;
}

inline double BetheBlochModel::Value(double xv, double x1, double x2,
                                     double y1, double y2) const {
    return y1 + (y2 - y1) * (xv - x1) / (x2 - x1);
}

inline double BetheBlochModel::Value2(double xv, double yv,
                                      double x1, double x2,
                                      double y1, double y2,
                                      double z11, double z21,
                                      double z12, double z22) const {
    return (z11 * (x2 - xv) * (y2 - yv) + z22 * (xv - x1) * (yv - y1) +
            z12 * (x2 - xv) * (yv - y1) + z21 * (xv - x1) * (y2 - yv))
           / ((x2 - x1) * (y2 - y1));
}


#endif
