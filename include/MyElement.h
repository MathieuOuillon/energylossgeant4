
#ifndef ENERGYLOSSGEANT4_MYELEMENT_H
#define ENERGYLOSSGEANT4_MYELEMENT_H

#include <string>

class MyElement {

public:  // with description

    //
    // Constructor to Build an element directly; no reference to isotopes
    //
    MyElement(const std::string& name,		//its name
              const std::string& symbol,		//its symbol
              double  Zeff,		//atomic number
              double  Aeff);		//mass of mole


    virtual ~MyElement();

    //
    // Retrieval methods
    //
    inline const std::string& GetName()   const {return fName;}
    inline const std::string& GetSymbol() const {return fSymbol;}

    // Atomic number
    inline double GetZ()             const {return fZeff;}
    inline int GetZasInt()           const {return fZ;}

    // Atomic weight in atomic units
    inline double GetN()             const {return fNeff;}
    inline double GetAtomicMassAmu() const {return fNeff;}

    // Mass of a mole in Geant4 units for atoms with atomic shell
    inline double GetA()             const {return fAeff;}

    //Coulomb correction factor:
    //
    inline double GetfCoulomb() const {return fCoulomb;}

    //Tsai formula for the radiation length:
    //
    inline double GetfRadTsai() const {return fRadTsai;}


public:  // without description

    inline void SetName(const std::string& name)  {fName=name;}

    MyElement(MyElement&) = delete;
    const MyElement & operator=(const MyElement&) = delete;
    bool operator==(const MyElement&) const = delete;
    bool operator!=(const MyElement&) const = delete;

private:

    void ComputeCoulombFactor();
    void ComputeLradTsaiFactor();

    //
    // Basic data members (which define an Element)
    //
    std::string fName;              // name
    std::string fSymbol;            // symbol
    double fZeff;              // Effective atomic number
    double fNeff;              // Effective number of nucleons
    double fAeff;              // Effective mass of a mole
    int    fZ;


    //
    // Derived data members (computed from the basic data members)
    //
    double fCoulomb;             // Coulomb correction factor
    double fRadTsai;             // Tsai formula for the radiation length
};

inline int Mylrint(double ad)
{
    return (ad > 0) ? static_cast<int>(ad + .5) : static_cast<int>(ad - .5);
}

#endif //ENERGYLOSSGEANT4_MYELEMENT_H
