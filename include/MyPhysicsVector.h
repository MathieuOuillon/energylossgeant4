//
// Created by ouillon-l on 14/03/2022.
//

#ifndef ENERGYLOSSGEANT4_MYPHYSICSVECTOR_H
#define ENERGYLOSSGEANT4_MYPHYSICSVECTOR_H

#include <fstream>
#include <iostream>
#include <vector>
#include <complex>

#include "MyPhysicsVectorType.h"

class MyPhysicsVector
{
public:
    // Default constructor - vector will be filled via Retrieve() method
    // Free vector may be filled via InsertValue(..) method
    explicit MyPhysicsVector(bool spline = false);

    virtual ~MyPhysicsVector() = default;

    // Get the cross-section/energy-loss value corresponding to the
    // given energy. An appropriate interpolation is used to calculate
    // the value. This method should be used if bin location cannot be
    // kept in the user code.
    inline double Value(const double e) const{
        double res;
        if(e > edgeMin && e < edgeMax)
        {
            const std::size_t idx = GetBin(e);
            res = Interpolation(idx, e);
        }
        else if(e <= edgeMin)
        {
            res = dataVector[0];
        }
        else
        {
            res = dataVector[numberOfNodes - 1];
        }
        return res;
    };

    // Returns the value in the energy specified by 'index'
    // of the energy vector. The boundary check will not be done.
    // Use this when compute cross-section, dEdx, or other value
    // before filling the vector by PutValue().
    inline double Energy(const std::size_t index) const{
        return binVector[index];
    };

    // Computes the lower index the energy bin in case of log-vector i.e.
    // in case of vectors with equal bin widths on log-scale
    // Note, that no check on the boundary is performed
    inline std::size_t ComputeLogVectorBin(const double logenergy) const{
        return std::min(static_cast<int>((logenergy - logemin) * invdBin), idxmax);
    };

protected:

    // The default implements a free vector initialisation.
    virtual void Initialise();

private:

    // Internal methods for computing of spline coeffitients

    // Linear or spline interpolation.
    inline double Interpolation(const std::size_t idx,
                                  const double e) const{
        // perform the interpolation
        const double x1 = binVector[idx];
        const double dl = binVector[idx + 1] - x1;

        const double y1 = dataVector[idx];
        const double dy = dataVector[idx + 1] - y1;

        // note: all corner cases of the previous methods are covered and eventually
        //       gives b=0/1 that results in y=y0\y_{N-1} if e<=x[0]/e>=x[N-1] or
        //       y=y_i/y_{i+1} if e<x[i]/e>=x[i+1] due to small numerical errors
        const double b = (e - x1) / dl;

        double res = y1 + b * dy;

        if(useSpline)  // spline interpolation
        {
            const double c0 = (2.0 - b) * secDerivative[idx];
            const double c1 = (1.0 + b) * secDerivative[idx + 1];
            res += (b * (b - 1.0)) * (c0 + c1) * (dl * dl * (1.0/6.0));
        }

        return res;
    };

    // Assuming (edgeMin <= energy <= edgeMax).
    inline std::size_t GetBin(const double e) const{
        std::size_t bin;
        switch(type)
        {
            case My_T_G4PhysicsLogVector:
                bin = ComputeLogVectorBin(std::log(e));
                break;

            case My_T_G4PhysicsLinearVector:
                bin = std::min(static_cast<int>((e - edgeMin) * invdBin), idxmax);
                break;

            default:
                // Bin location proposed by K.Genser (FNAL)
                bin = std::lower_bound(binVector.begin(), binVector.end(), e) -
                      binVector.begin() - 1;
        }
        return bin;
    };

protected:

    double edgeMin = 0.0;  // Energy of first point
    double edgeMax = 0.0;  // Energy of the last point

    double invdBin = 0.0;  // 1/Bin width for linear and log vectors
    double logemin = 0.0;  // used only for log vector

    int idxmax = 0;
    std::size_t numberOfNodes = 0;

    MyPhysicsVectorType type = My_T_G4PhysicsFreeVector;
    // The type of PhysicsVector (enumerator)

    std::vector<double> binVector;      // energy
    std::vector<double> dataVector;     // crossection/energyloss
    std::vector<double> secDerivative;  // second derivatives

private:

    bool useSpline = false;
};


#endif
