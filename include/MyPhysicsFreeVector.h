//
// Created by ouillon-l on 14/03/2022.
//

#ifndef ENERGYLOSSGEANT4_MYPHYSICSFREEVECTOR_H
#define ENERGYLOSSGEANT4_MYPHYSICSFREEVECTOR_H

#include "MyPhysicsVector.h"

class MyPhysicsFreeVector : public MyPhysicsVector {
public:

    explicit MyPhysicsFreeVector(std::size_t length, bool spline = false);

    ~MyPhysicsFreeVector() override = default;

    // Filling of the vector with the check on index and energy
    void PutValues(std::size_t index, double energy, double value);

};

#endif //ENERGYLOSSGEANT4_MYPHYSICSFREEVECTOR_H
