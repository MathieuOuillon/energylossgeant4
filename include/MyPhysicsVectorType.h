
#ifndef ENERGYLOSSGEANT4_MYPHYSICSVECTORTYPE_H
#define ENERGYLOSSGEANT4_MYPHYSICSVECTORTYPE_H

enum MyPhysicsVectorType
{
    My_T_G4PhysicsFreeVector = 0,
    My_T_G4PhysicsLinearVector,
    My_T_G4PhysicsLogVector
};

#endif
