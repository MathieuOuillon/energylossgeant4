#include <iostream>
#include <array>
#include "include/BetheBlochModel.h"
#include "include/MySystemOfUnits.h"


#include <G4NistManager.hh>
#include <G4Proton.hh>
#include <G4VModularPhysicsList.hh>
#include <G4RunManagerFactory.hh>
#include <FTFP_BERT.hh>
#include "G4SystemOfUnits.hh"
#include "G4BetheBlochModel.hh"
#include "G4ParticleTable.hh"
#include "include/MaterialBuilder.h"

std::array<double, 2> energyLoss(Particle *proton, Material *mat, double kineticEnergy);

int main() {

    std::cout.precision(18);

    // auto *proton = new Particle("Proton", 938.272013 * MyCLHEP::MeV, 1, MyCLHEP::eplus);
    auto *proton = new Proton();


    auto *builder = new MaterialBuilder();
    Material *CO2 = builder->FindMaterial("CARBON_DIOXIDE");
    Material *He = builder->FindMaterial("G4_He");
    Material *BONuSGas = builder->FindMaterial("BONuSGas");
    Material *Kapton = builder->FindMaterial("Kapton");
    Material *Mylar = builder->FindMaterial("Mylar");

    // ------------------------------------------------------------ //
    // ------------------------------------------------------------ //

    setenv("G4ENSDFSTATEDATA", "/home/ouillon-l/GEANT4/G4ENSDFSTATE2.3/", 0);

    auto *runManager = G4RunManagerFactory::CreateRunManager(G4RunManagerType::Default);
    G4VModularPhysicsList *physicsList = new FTFP_BERT;
    physicsList->SetVerboseLevel(0);
    runManager->SetUserInitialization(physicsList);

    G4NistManager *man = G4NistManager::Instance();
    G4Material *CO2_GEANT4 = man->FindOrBuildMaterial("G4_CARBON_DIOXIDE");
    G4Material *He_GEANT4 = man->FindOrBuildMaterial("G4_He");
    G4Material *Kapton_GEANT4 = man->FindOrBuildMaterial("G4_KAPTON");
    G4Material *Mylar_GEANT4 = man->FindOrBuildMaterial("G4_MYLAR");

    double density_BONuSGas_GEANT4 = 0.539 * mg / cm3;
    auto *BONuSGas_GEANT4 = new G4Material("BONuSGas", density_BONuSGas_GEANT4, 2);
    BONuSGas_GEANT4->AddMaterial(CO2_GEANT4, 73.32 * perCent);
    BONuSGas_GEANT4->AddMaterial(He_GEANT4, 26.67 * perCent);

    G4ParticleTable *particleTable = G4ParticleTable::GetParticleTable();
    G4ParticleDefinition *p = particleTable->FindParticle("proton");


    // ------------------------------------------------------------ //
    // ------------------------------------------------------------ //

    double kinEnergy = 2.602;
    auto *model = new G4BetheBlochModel();

    std::array<double, 2> dedx_CO2 = energyLoss(proton, CO2, kinEnergy);
    double dedx_CO2_GEANT4 =
            model->ComputeDEDXPerVolume(CO2_GEANT4, p, kinEnergy, 1e6) * 10 / (CO2_GEANT4->GetDensity() / g * cm3);

    std::cout << "T = " << kinEnergy << std::endl;
    std::cout << "------------------------------------------------------" << std::endl;
    std::cout << "density_CO2 = " << CO2->getDensity() << " g/cm^3" << std::endl;
    std::cout << "dedx_wo_CO2_calc = " << dedx_CO2[0] / CO2->getDensity() * 1000 << " MeV•g/cm^2" << std::endl;
    std::cout << "dedx_CO2_calc = " << dedx_CO2[1] / CO2->getDensity() * 1000 << " MeV•g/cm^2" << std::endl;
    std::cout << "dedx_CO2_GEANT4 = " << dedx_CO2_GEANT4 << " MeV•g/cm^2" << std::endl;
    std::cout << "dedx_CO2_NIST = " << 1.135E+02 << " MeV•g/cm^2" << std::endl;

    std::array<double, 2> dedx_He = energyLoss(proton, He, kinEnergy);
    double dedx_He_GEANT4 =
            model->ComputeDEDXPerVolume(He_GEANT4, p, kinEnergy, 1e6) * 10 / (He_GEANT4->GetDensity() / g * cm3);

    std::cout << "------------------------------------------------------" << std::endl;
    std::cout << "density_He = " << He->getDensity() << " g/cm^3" << std::endl;
    std::cout << "dedx_wo_He_calc = " << dedx_He[0] / He->getDensity() * 1000 << " MeV•g/cm^2" << std::endl;
    std::cout << "dedx_He_calc = " << dedx_He[1] / He->getDensity() * 1000 << " MeV•g/cm^2" << std::endl;
    std::cout << "dedx_He_GEANT4 = " << dedx_He_GEANT4 << " MeV•g/cm^2" << std::endl;
    std::cout << "dedx_He_NIST = " << 1.359E+02 << " MeV•g/cm^2" << std::endl;

    std::array<double, 2> dedx_BONuSGas = energyLoss(proton, BONuSGas, kinEnergy);
    double dedx_BONuSGas_GEANT4 = model->ComputeDEDXPerVolume(BONuSGas_GEANT4, p, kinEnergy, 1e6) * 10 /
                                  (BONuSGas_GEANT4->GetDensity() / g * cm3);
    std::cout << "------------------------------------------------------" << std::endl;
    std::cout << "density_BONuSGas = " << BONuSGas->getDensity() << " g/cm^3" << std::endl;
    std::cout << "dedx_wo_BONuSGas_calc = " << dedx_BONuSGas[0] / BONuSGas->getDensity() * 1000 << " MeV•g/cm^2"
              << std::endl;
    std::cout << "dedx_BONuSGas_calc = " << dedx_BONuSGas[1] / BONuSGas->getDensity() * 1000 << " MeV•g/cm^2"
              << std::endl;
    std::cout << "dedx_BONuSGas_GEANT4 = " << dedx_BONuSGas_GEANT4 << " MeV•g/cm^2" << std::endl;
    std::cout << "dedx_BONuSGas_NIST = " << 0.7332 * 1.135E+02 + 0.2667 * 1.359E+02 << " MeV•g/cm^2" << std::endl;


    std::array<double, 2> dedx_Kapton = energyLoss(proton, Kapton, kinEnergy);
    double dedx_Kapton_GEANT4 = model->ComputeDEDXPerVolume(Kapton_GEANT4, p, kinEnergy, 1e6) * 10 /
                                (Kapton_GEANT4->GetDensity() / g * cm3);
    std::cout << "------------------------------------------------------" << std::endl;
    std::cout << "density_Kapton = " << Kapton->getDensity() << " g/cm^3" << std::endl;
    std::cout << "dedx_wo_Kapton_calc = " << dedx_Kapton[0] / Kapton->getDensity() * 1000 << " MeV•g/cm^2"
              << std::endl;
    std::cout << "dedx_Kapton_calc = " << dedx_Kapton[1] / Kapton->getDensity() * 1000 << " MeV•g/cm^2"
              << std::endl;
    std::cout << "dedx_Kapton_GEANT4 = " << dedx_Kapton_GEANT4 << " MeV•g/cm^2" << std::endl;
    std::cout << "dedx_Kapton_NIST = " << 1.186E+02 << " MeV•g/cm^2" << std::endl;


    std::array<double, 2> dedx_Mylar = energyLoss(proton, Mylar, kinEnergy);
    double dedx_Mylar_GEANT4 = model->ComputeDEDXPerVolume(Mylar_GEANT4, p, kinEnergy, 1e6) * 10 /
                               (Mylar_GEANT4->GetDensity() / g * cm3);
    std::cout << "------------------------------------------------------" << std::endl;
    std::cout << "density_Mylar = " << Mylar->getDensity() << " g/cm^3" << std::endl;
    std::cout << "dedx_wo_Mylar_calc = " << dedx_Mylar[0] / Mylar->getDensity() * 1000 << " MeV•g/cm^2"
              << std::endl;
    std::cout << "dedx_Mylar_calc = " << dedx_Mylar[1] / Mylar->getDensity() * 1000 << " MeV•g/cm^2"
              << std::endl;
    std::cout << "dedx_Mylar_GEANT4 = " << dedx_Mylar_GEANT4 << " MeV•g/cm^2" << std::endl;
    std::cout << "dedx_Mylar_NIST = " << 1.208E+02 << " MeV•g/cm^2" << std::endl;


    return EXIT_SUCCESS;
}


std::array<double, 2> energyLoss(Particle *proton, Material *mat, double kineticEnergy) {

    auto *model = new BetheBlochModel();

    double dedx_wo_corr = model->ComputeDEDXPerVolume(proton, mat, kineticEnergy, false);
    double dedx_w_corr = model->ComputeDEDXPerVolume(proton, mat, kineticEnergy, true);

    return std::array<double, 2>{dedx_wo_corr, dedx_w_corr};


}

