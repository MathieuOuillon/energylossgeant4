
#include <complex>
#include "../include/ElementBuilder.h"


using namespace MyCLHEP;

ElementBuilder::ElementBuilder() {
    Initialise();
}

void ElementBuilder::Initialise() {

    bindingEnergy[0] = 0.0;
    for (int i = 1; i < MymaxNumElements; ++i) {
        auto Z = double(i);
        bindingEnergy[i] = (14.4381 * std::pow(Z, 2.39) + 1.55468e-6 * std::pow(Z, 5.35)) * eV;
    }

    BuildHydrogen();
    BuildHelium();
    BuildCarbon();
    BuildNitrogen();
    BuildOxygen();

}

MyElement *ElementBuilder::FindElement(const std::string &nameElm) {
    for (auto & it : ElementMap) {
        if (it.first == nameElm) { return it.second; }
    }
}


void ElementBuilder::AddElement(const std::string &name, const std::string &symbol, double Zeff, const double &A,
                                const double &W) {
    double www;
    double Aeff = 0;
    int nc = 6;

    for (int i = 0; i < nc; ++i) {
        www = 0.01 * (&W)[i];
        Aeff += www * (&A)[i];
    }

    auto *elm = new MyElement(name, symbol, Zeff, Aeff);
    ElementMap[elm->GetName()] = elm;
}

void ElementBuilder::BuildHydrogen() {
    double A[6] = {1.00783, 2.0141, 3.01605, 4.02783, 5.03954, 6.04494};
    A[0] = (proton_mass_c2 + electron_mass_c2 - bindingEnergy[1]) / amu_c2;
    A[1] = (1.875613 * GeV + electron_mass_c2 - bindingEnergy[1]) / amu_c2;
    A[2] = (2.80925 * GeV + electron_mass_c2 - bindingEnergy[1]) / amu_c2;
    double W[6] = {99.9885, 0.0115, 0, 0, 0, 0};


    AddElement("Hydrogen", "H", 1., *A, *W);

}


void ElementBuilder::BuildHelium() {
    double A[8] = {3.01603, 4.0026, 5.01222, 6.01889, 7.02803, 8.03392, 9.04382, 10.0524};
    A[0] = (2.80923 * GeV + 2.0 * electron_mass_c2 - bindingEnergy[2]) / amu_c2;
    A[1] = (3.727417 * GeV + 2.0 * electron_mass_c2 - bindingEnergy[2]) / amu_c2;
    double W[8] = {0.000137, 99.9999, 0, 0, 0, 0, 0, 0};

    AddElement("Helium", "He", 2, *A, *W);
}

void ElementBuilder::BuildCarbon() {
    double A[15] = {8.03768, 9.03104, 10.0169, 11.0114, 12., 13.0034, 14.0032, 15.0106,
                    16.0147, 17.0226, 18.0268, 19.0353, 20.0403, 21.0493, 22.0565};
    double W[15] = {0, 0, 0, 0, 98.93, 1.07, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    AddElement("Carbon", "C", 6, *A, *W);

}

void ElementBuilder::BuildNitrogen() {
    double A[15] = {10.0426, 11.0268, 12.0186, 13.0057, 14.0031, 15.0001, 16.0061, 17.0084,
                    18.0141, 19.017, 20.0234, 21.0271, 22.0344, 23.0405, 24.0505};
    double W[15] = {0, 0, 0, 0, 99.632, 0.368, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    AddElement("Nitrogen", "N", 7, *A, *W);

}

void ElementBuilder::BuildOxygen() {
    double A[15] = {12.0344, 13.0248, 14.0086, 15.0031, 15.9949, 16.9991, 17.9992, 19.0036,
                    20.0041, 21.0087, 22.01, 23.0157, 24.0204, 25.0291, 26.0377};
    double W[15] = {0, 0, 0, 0, 99.757, 0.038, 0.205, 0, 0, 0, 0, 0, 0, 0, 0};

    AddElement("Oxygen", "O", 8, *A, *W);

}




