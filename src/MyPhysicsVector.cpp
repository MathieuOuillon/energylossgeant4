//
// Created by ouillon-l on 14/03/2022.
//


#include "../include/MyPhysicsVector.h"
#include <iomanip>

// --------------------------------------------------------------
MyPhysicsVector::MyPhysicsVector(bool val)
        : useSpline(val)
{}

// --------------------------------------------------------------------
void MyPhysicsVector::Initialise()
{
    idxmax = numberOfNodes - 2;
    if(0 < numberOfNodes)
    {
        edgeMin = binVector[0];
        edgeMax = binVector[numberOfNodes - 1];
    }
}

//---------------------------------------------------------------
