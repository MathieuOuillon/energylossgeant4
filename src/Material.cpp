//
// Created by ouillon-l on 14/03/2022.
//

#include "../include/Material.h"
#include "../include/MyPhysicalConstants.h"


Material::Material(const std::string &name, double density, int nComponents, double I, double n_el, double temp, double pressure) : fName(name) {
    fDensity  = density;
    fTemp     = temp;
    fPressure = pressure;
    fNbComponents = nComponents;
    fI = I;
    fElectronDensity = n_el;
}

void Material::setIonisParamMat(double *params) {
    fCdensity = params[0];
    fX0density = params[1];
    fX1density = params[2];
    fAdensity = params[3];
    fMdensity = params[4];
    fD0density = params[5];
}
