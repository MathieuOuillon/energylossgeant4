//
// Created by ouillon-l on 22/03/2022.
//

#include "../include/MaterialBuilder.h"

MaterialBuilder::MaterialBuilder() {
    Initialise();
}

void MaterialBuilder::Initialise() {

    elementBuilder = new ElementBuilder();

    BuildHelium();
    BuildCarbonDioxide();
    BuildBONuSGas();
    BuildKapton();
    BuildMylar();

}

void MaterialBuilder::AddMaterial(const std::string &nameMat, double dens, std::vector<MyElement *> elmVec,
                                  double Imat, int ncomp, double params[6], double n_el,
                                  std::vector<double> atomicDensity) {
    auto *mat = new Material(nameMat, dens, ncomp, Imat, n_el);
    mat->setMyElementVector(elmVec);
    mat->setIonisParamMat(params);
    mat->setAtomicDensity(atomicDensity);
    materialMap[nameMat] = mat;
}

Material *MaterialBuilder::FindMaterial(const std::string &nameMat) {
    for (auto &it: materialMap) {
        if (it.first == nameMat) { return it.second; }
    }
}

void MaterialBuilder::BuildHelium() {
    // Helium
    double density = 1.66322e-4; //g/cm3
    double I = 41.8; // eV
    double ionisParams[6] = {11.1393, 2.2017, 3.6122, 0.13443, 5.8347, 0}; // ionisParams[6]:  -C X_0 X_1 a m delta_0
    double nel = 50047660973619752.;
    std::vector<MyElement *> elmVec = {elementBuilder->FindElement("Helium")};
    // std::vector<double> atomicDensityVec = {MyCLHEP::Avogadro * 1.66322e-4 * 1. / 4.};
    std::vector<double> atomicDensityVec = {2.502383049e+16};
    AddMaterial("G4_He", density, elmVec, I, 1, ionisParams, nel, atomicDensityVec);

}

void MaterialBuilder::BuildCarbonDioxide() {
    // CARBON_DIOXIDE
    double density = 0.00184212; //g/cm3
    double I = 85.; // eV
    double ionisParams[6] = {10.1537, 1.6294, 4.1825, 0.11768, 3.3227, 0}; // Data[10]:  -C X_0 X_1 a m delta_0
    double nel = 554555346493906304.;
    std::vector<MyElement *> elmVec = {elementBuilder->FindElement("Carbon"),
                                       elementBuilder->FindElement("Oxygen")};
    // std::vector<double> atomicDensityVec = {MyCLHEP::Avogadro * density * 0.2729 / 12.011,
    //                                         MyCLHEP::Avogadro * density * 0.727084 / 15.999};
    std::vector<double> atomicDensityVec = {2.52070612e+16,5.041412241e+16};
    AddMaterial("CARBON_DIOXIDE", density, elmVec, I, 2, ionisParams, nel, atomicDensityVec);

}

void MaterialBuilder::BuildBONuSGas() {
    double density = 0.539e-3; // g/cm3
    double I = 73.92744031;
    double ionisParams[6] = {11.10358775, 1.9, 4, 0.2541587725, 3, 0}; // Data[10]:  -C X_0 X_1 a m delta_0
    double nel = 1.622423779e+17;
    std::vector<MyElement *> elmVec = {elementBuilder->FindElement("Carbon"),
                                       elementBuilder->FindElement("Oxygen"),
                                       elementBuilder->FindElement("Helium")};
    // std::vector<double> atomicDensityVec = {0.2667 * MyCLHEP::Avogadro * density / 4.,
    //                                         0.2001 * MyCLHEP::Avogadro * density / 12.011,
    //                                         0.5332 * MyCLHEP::Avogadro * density / 15.999};
    std::vector<double> atomicDensityVec = {5.4082773e+15,1.08165546e+16,2.163013865e+16};
    AddMaterial("BONuSGas", density, elmVec, I, 3, ionisParams, nel, atomicDensityVec);
}

void MaterialBuilder::BuildKapton() {
    double density = 1.420; // g/cm3
    double I = 79.600;
    double ionisParams[6] = {3.3497, 0.1509, 2.5631,  0.1597081102, 3.1921, 0}; // Data[10]:  -C X_0 X_1 a m delta_0
    double nel = 4.3839074175096134e+20;
    std::vector<MyElement *> elmVec = {elementBuilder->FindElement("Carbon"),
                                       elementBuilder->FindElement("Hydrogen"),
                                       elementBuilder->FindElement("Nitrogen"),
                                       elementBuilder->FindElement("Oxygen")};
    // std::vector<double> atomicDensityVec = {0.691133 * MyCLHEP::Avogadro * density / elmVec[0]->GetA(),
    //                                         0.026362 * MyCLHEP::Avogadro * density / elmVec[1]->GetA(),
    //                                         0.073270 * MyCLHEP::Avogadro * density / elmVec[2]->GetA(),
    //                                         0.209235 * MyCLHEP::Avogadro * density / elmVec[3]->GetA()};

    std::vector<double> atomicDensityVec = {4.920712407e+19,2.236687458e+19,4.473374916e+18,1.118343729e+19};
    AddMaterial("Kapton", density, elmVec, I, 4, ionisParams, nel, atomicDensityVec);

}

void MaterialBuilder::BuildMylar() {
    double density = 1.400; // g/cm3
    double I = 78.700;
    double ionisParams[6] = {3.3262, 0.1562, 2.6507,  0.12678, 3.3076, 0}; // Data[10]:  -C X_0 X_1 a m delta_0
    double nel = 4.38729510474380018e+20;
    std::vector<MyElement *> elmVec = {elementBuilder->FindElement("Carbon"),
                                       elementBuilder->FindElement("Hydrogen"),
                                       elementBuilder->FindElement("Oxygen")};
    // std::vector<double> atomicDensityVec = {0.625017 * MyCLHEP::Avogadro * density / elmVec[0]->GetA(),
    //                                         0.041959 * MyCLHEP::Avogadro * density / elmVec[1]->GetA(),
    //                                         0.333025 * MyCLHEP::Avogadro * density / elmVec[2]->GetA()};
    std::vector<double> atomicDensityVec = {4.387295105e+19,3.509836084e+19,1.754918042e+19};
    AddMaterial("Mylar", density, elmVec, I, 3, ionisParams, nel, atomicDensityVec);

}

