//
// Created by ouillon-l on 22/03/2022.
//

#include <complex>
#include "../include/MyElement.h"
#include "../include/MySystemOfUnits.h"
#include "../include/MyPhysicalConstants.h"

using namespace MyCLHEP;

MyElement::MyElement(const std::string &name, const std::string &symbol,
                     double zeff, double aeff)
        : fName(name), fSymbol(symbol), fCoulomb(0), fRadTsai(0) {
    fZeff = zeff;
    fAeff = aeff;
    fNeff = fAeff / (g / mole);

    if (fNeff < 1.0) fNeff = 1.0;

    // Radiation Length
    ComputeCoulombFactor();
    ComputeLradTsaiFactor();

    fZ = Mylrint(fZeff);
}


MyElement::~MyElement() {}


void MyElement::ComputeCoulombFactor() {
    //
    //  Compute Coulomb correction factor (Phys Rev. D50 3-1 (1994) page 1254)

    static const double k1 = 0.0083, k2 = 0.20206, k3 = 0.0020, k4 = 0.0369;

    double az2 = (fine_structure_const * fZeff) * (fine_structure_const * fZeff);
    double az4 = az2 * az2;

    fCoulomb = (k1 * az4 + k2 + 1. / (1. + az2)) * az2 - (k3 * az4 + k4) * az4;
}


void MyElement::ComputeLradTsaiFactor() {
    //  Compute Tsai's Expression for the Radiation Length
    //  (Phys Rev. D50 3-1 (1994) page 1254)

    static const double Lrad_light[] = {5.31, 4.79, 4.74, 4.71};
    static const double Lprad_light[] = {6.144, 5.621, 5.805, 5.924};

    const double logZ3 = std::log(fZeff) / 3.;

    double Lrad, Lprad;
    int iz = Mylrint(fZeff) - 1;
    static const double log184 = std::log(184.15);
    static const double log1194 = std::log(1194.);
    if (iz <= 3) {
        Lrad = Lrad_light[iz];
        Lprad = Lprad_light[iz];
    } else {
        Lrad = log184 - logZ3;
        Lprad = log1194 - 2 * logZ3;
    }

    fRadTsai = 4 * alpha_rcl2 * fZeff * (fZeff * (Lrad - fCoulomb) + Lprad);
}
