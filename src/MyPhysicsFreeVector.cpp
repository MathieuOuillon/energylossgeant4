#include "../include/MyPhysicsFreeVector.h"

// --------------------------------------------------------------------
MyPhysicsFreeVector::MyPhysicsFreeVector(std::size_t length, bool spline)
        : MyPhysicsVector(spline) {
    numberOfNodes = length;

    if (0 < length) {
        binVector.resize(numberOfNodes, 0.0);
        dataVector.resize(numberOfNodes, 0.0);
    }
    Initialise();
}

// --------------------------------------------------------------------
void MyPhysicsFreeVector::PutValues(const std::size_t index,
                                    const double e,
                                    const double value) {
    if (index >= numberOfNodes) {
        return;
    }
    binVector[index] = e;
    dataVector[index] = value;
    if (index == 0) {
        edgeMin = e;
    } else if (numberOfNodes == index + 1) {
        edgeMax = e;
    }
}

// --------------------------------------------------------------------
