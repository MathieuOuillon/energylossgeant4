
#include "../include/Particle.h"

#include <utility>
#include "../include/MyPhysicalConstants.h"

Particle::Particle(std::string aName,
                   double mass,
                   double spin,
                   double charge)
        :
        theParticleName(std::move(aName)),
        theMass(mass),
        theCharge(charge),
        theSpin(spin) {}


